#!/bin/sh
python pmnist_clean_train.py --optim adam --wt_para 0.22
for i in {0..3}; do
    python pmnist_clean_train.py --restart_tsk $(($i+1)) --load_model True --model_pth "./mlp_exp1pmnist_task_${i}.pt" \
    --turn_off True --optim adam --wt_para 0.2
    rm mlp_exp1pmnist_task_${i}.pt
done
for i in {4..6}; do
    python pmnist_clean_train.py --restart_tsk $(($i+1)) --load_model True --model_pth "./mlp_exp1pmnist_task_${i}.pt" \
    --turn_off True --optim adam --wt_para 0.15
    rm mlp_exp1pmnist_task_${i}.pt
done

python pmnist_clean_train.py --restart_tsk 8 --load_model True --model_pth "./mlp_exp1pmnist_task_7.pt" \
--turn_off True --optim adam --wt_para 1e-1

python pmnist_clean_train.py --restart_tsk 9 --load_model True --model_pth "./mlp_exp1pmnist_task_8.pt" \
--turn_off True --optim adam --wt_para 1e-2

for i in {7..8}; do
    rm mlp_exp1pmnist_task_${i}.pt
    rm ./permutations.pt
done

mv mlp_exp1pmnist_task_9.pt ../results/