import os
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torch
import torch.utils.data as data
import numpy as np

# add randomaffine?
def data_create(opt):

    if opt.dataset == 'mnist' or 'pmnist':
        dataset = dset.MNIST(root=opt.dataroot, download=True, train=True,
                                    transform=transforms.Compose([
                                        transforms.Resize(opt.im_size),
                                        transforms.RandomApply([transforms.RandomAffine(degrees=(-10, 10), \
                                            scale=(0.8, 1.2), translate=(0.05, 0.05))],p=0.5),
                                        transforms.ToTensor(),
                                        transforms.Normalize((0.1307,), (0.3081,)),
                                    ]))
        valset = dset.MNIST(root=opt.dataroot, download=True, train=False,
                                   transform=transforms.Compose([
                                       transforms.Resize(opt.im_size),
                                       transforms.ToTensor(),
                                       transforms.Normalize((0.1307,), (0.3081,)),
                                   ]))

    if opt.dataset == 'fmnist':
        dataset = dset.FashionMNIST(root=opt.dataroot, download=True, train=True,
                                    transform=transforms.Compose([
                                        transforms.Resize(opt.im_size),
                                        transforms.ToTensor(),
                                        transforms.Normalize((0.1307,), (0.3081,)),
                                    ]))
        valset = dset.FashionMNIST(root=opt.dataroot, download=True, train=False,
                                   transform=transforms.Compose([
                                       transforms.Resize(opt.im_size),
                                       transforms.ToTensor(),
                                       transforms.Normalize((0.1307,), (0.3081,)),
                                   ]))

    if opt.dataset == 'svhn':
        dataset = dset.SVHN(root=opt.dataroot, download=True, split='train',
                            transform=transforms.Compose([transforms.Resize(opt.im_size), transforms.ToTensor(),
                                                          transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]))
        valset = dset.SVHN(root=opt.dataroot, download=True, split='test',
                           transform=transforms.Compose([transforms.Resize(opt.im_size), transforms.ToTensor(),
                                                         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]))

    elif opt.dataset in ['imagenet', 'folder', 'lfw']:
        # folder dataset
        dataset = dset.ImageFolder(root=opt.dataroot,
                                   transform=transforms.Compose([
                                       transforms.Resize(opt.im_size),
                                       transforms.CenterCrop(opt.im_size),
                                       transforms.ToTensor(),
                                       transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                                   ]))

    elif opt.dataset == 'lsun':
        dataset = dset.LSUN(db_path=opt.dataroot, classes=['bedroom_train'],
                            transform=transforms.Compose([
                                transforms.Resize(opt.im_size),
                                transforms.CenterCrop(opt.im_size),
                                transforms.ToTensor(),
                                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                            ]))

    elif opt.dataset == 'cifar10':
        dataset = dset.CIFAR10(root=opt.dataroot, download=True,
                               transform=transforms.Compose([
                               transforms.RandomCrop(32, padding=4),
                               transforms.RandomHorizontalFlip(),
                                transforms.ToTensor(),
                                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
                                # transforms.Lambda(lambda x: add_noise(x))
                                ]))
        valset = dset.CIFAR10(root=opt.dataroot, download=True, train=False,
                              transform=transforms.Compose([
                                transforms.ToTensor(),
                                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
                                # transforms.Lambda(lambda x: add_noise(x))
                                ]))

    elif opt.dataset == 'cifar100':
        dataset = dset.CIFAR100(root=opt.dataroot, download=False,
                                transform=transforms.Compose([
                                  transforms.RandomCrop(32, padding=4),
                                  transforms.RandomHorizontalFlip(),
                                  transforms.ToTensor(),
                                  transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
                                  # transforms.Lambda(lambda x: add_noise(x))
                                ]))
        valset = dset.CIFAR100(root=opt.dataroot, download=False, train=False,
                               transform=transforms.Compose([
                                transforms.ToTensor(),
                                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
                                # transforms.Lambda(lambda x: add_noise(x))
                                ]))

    return dataset, valset


def create_dataloader(dataset, valset, opt, val_only=False):
    if not val_only:
        if opt.dataset == 'fmnist' or 'pmnist':
            dataloader = torch.utils.data.DataLoader(dataset, batch_size=opt.batch_size,
                                                    shuffle=True, num_workers=int(opt.workers))
        else:
            dataloader = torch.utils.data.DataLoader(dataset, batch_size=opt.batch_size,
                                                    shuffle=True, num_workers=int(opt.workers), drop_last=True)

    testloader = torch.utils.data.DataLoader(valset, batch_size=opt.batch_size,
                                                 shuffle=False, num_workers=int(opt.workers))#, drop_last=True)  # check this
    # print(len(dataloader[0]))
    if val_only:
        return testloader
    return dataloader, testloader

def get_split_cifar100(opt, task_id, class_size=5, shuffle=False):
    start_class = (task_id-1)*class_size
    end_class = task_id*class_size

    if opt.dataset == 's-cifar100':
        opt.dataset='cifar100'
    train, test = data_create(opt)
    targets_train = torch.tensor(train.targets)
    target_train_idx = ((targets_train >= start_class) & (targets_train < end_class))

    targets_test = torch.tensor(test.targets)
    target_test_idx = ((targets_test >= start_class) & (targets_test < end_class))

    trainloader = torch.utils.data.DataLoader(torch.utils.data.dataset.Subset(train, np.where(target_train_idx==1)[0]),\
         batch_size=opt.batch_size, shuffle=True, num_workers=int(opt.workers), drop_last=True)  # check this
    testloader = torch.utils.data.DataLoader(torch.utils.data.dataset.Subset(test, np.where(target_test_idx==1)[0]),\
         batch_size=opt.batch_size, shuffle=False, num_workers=int(opt.workers), drop_last=True)  # check this
    if opt.dataset == 'cifar100':
        opt.dataset='s-cifar100'
    return trainloader, testloader

def get_random_cifar100(opt, task_id, class_size=5, shuffle=False):
    """needs fixing"""
    start_class = 0
    end_class = task_id*class_size

    if opt.dataset == 's-cifar100':
        opt.dataset='cifar100'
    train, test = data_create(opt)
    targets_train = torch.tensor(train.targets)
    target_train_idx = (targets_train < end_class)

    class_sample_count = np.unique(targets_train[target_train_idx==1], return_counts=True)[1]
    print(class_sample_count,sum(class_sample_count))
    exit()
    # oversample class 0
    if task_id>1:
        for ix in range((task_id-1)*class_size, end_class):
            class_sample_count[ix] = 50

    weight = 1. / class_sample_count
    samples_weight = weight[target]
    samples_weight = torch.from_numpy(samples_weight)
    sampler = WeightedRandomSampler(samples_weight, len(samples_weight))

    targets_test = torch.tensor(test.targets)
    target_test_idx = (targets_test < end_class)

    trainloader = torch.utils.data.DataLoader(torch.utils.data.dataset.Subset(train, np.where(target_train_idx==1)[0]),\
         batch_size=opt.batch_size, shuffle=True, num_workers=int(opt.workers), drop_last=True)  # check this
    testloader = torch.utils.data.DataLoader(torch.utils.data.dataset.Subset(test, np.where(target_test_idx==1)[0]),\
         batch_size=opt.batch_size, shuffle=False, num_workers=int(opt.workers), drop_last=True)  # check this
    if opt.dataset == 'cifar100':
        opt.dataset='s-cifar100'
    return trainloader, testloader


def get_two_cifar100(opt, task_id, class_size=5, shuffle=False):
    """Gives two pair of dataloaders - current task and a mix of old tasks"""
    assert(opt.batch_size%2==0)
    start_class = (task_id-1)*class_size
    end_class = task_id*class_size

    if opt.dataset == 's-cifar100':
        opt.dataset='cifar100'
    train, test = data_create(opt)
    targets_train = torch.tensor(train.targets)
    target_train_idx = ((targets_train >= start_class) & (targets_train < end_class))

    targets_test = torch.tensor(test.targets)
    target_test_idx = ((targets_test >= start_class) & (targets_test < end_class))

    trainloader = torch.utils.data.DataLoader(torch.utils.data.dataset.Subset(train, np.where(target_train_idx==1)[0]),\
         batch_size=int(opt.batch_size/2), shuffle=True, num_workers=int(opt.workers), drop_last=True)  # check this
    testloader = torch.utils.data.DataLoader(torch.utils.data.dataset.Subset(test, np.where(target_test_idx==1)[0]),\
         batch_size=int(opt.batch_size/2), shuffle=False, num_workers=int(opt.workers), drop_last=True)  # check this
    
    end_class = (task_id-1)*class_size
    targets_train = torch.tensor(train.targets)
    target_train_idx = (targets_train < end_class)

    targets_test = torch.tensor(test.targets)
    target_test_idx = (targets_test < end_class)

    pretrainloader = torch.utils.data.DataLoader(torch.utils.data.dataset.Subset(train, np.where(target_train_idx==1)[0]),\
         batch_size=int(opt.batch_size), shuffle=True, num_workers=int(opt.workers), drop_last=True)  # check this
    pretestloader = torch.utils.data.DataLoader(torch.utils.data.dataset.Subset(test, np.where(target_test_idx==1)[0]),\
         batch_size=int(opt.batch_size), shuffle=False, num_workers=int(opt.workers), drop_last=True)  # check this
    
    return trainloader, testloader, pretrainloader, pretestloader
    # return trainloader, testloader, pretrainloader, pretestloader


def imagenet50_data(opt, dataroot):
    TRANSFORM_IMG = transforms.Compose([
    transforms.Resize(opt.im_size),
    transforms.CenterCrop(opt.im_size),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                         std=[0.229, 0.224, 0.225] )
    ])

    TRANSFORM_IMG2 = transforms.Compose([
    transforms.Resize(opt.im_size),
    transforms.CenterCrop(opt.im_size),
    # transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                         std=[0.229, 0.224, 0.225] )
    ])

    train_pth = dataroot+'/train'
    test_pth = dataroot+'/val'

    train_data = dset.ImageFolder(root=train_pth, transform=TRANSFORM_IMG)
    train_data_loader = data.DataLoader(train_data, batch_size=opt.batch_size, shuffle=True,  num_workers=opt.workers)
    test_data = dset.ImageFolder(root=test_pth, transform=TRANSFORM_IMG2)
    test_data_loader  = data.DataLoader(test_data, batch_size=1, shuffle=False, num_workers=opt.workers) 

    return train_data_loader, test_data_loader