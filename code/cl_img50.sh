#!/bin/sh

python train_imagenet.py --lr 1e-3 --epochs 150 --prune_epoch 50 --lr_adj 1e-2 --wt_para .05 --turn_off True \
--name exp1_ --optim adam --train_p -1 --dataroot "/mnt/sda1/Data/Imagenet50-Cl/task1"

for i in {0..3}; do
    python train_imagenet.py --lr 1e-3 --epochs 100 --prune_epoch 40 --lr_adj 1e-2 \
    --dataroot "/mnt/sda1/Data/Imagenet50-Cl/task${i+2}" \
    --wt_para 1e-2 --restart_tsk $(($i+1)) --load_model True --model_pth "./exp1_imagenet50_task_${i}.pt" \
    --turn_off True --name exp1_ --optim adam --train_p 90
done

mv exp1_imagenet50_task_4.pt ../results/
for i in {0..3}; do
    rm exp1_imagenet50_task_${i}.pt
done