#!/bin/sh

python clean_train.py --dataset cifar100 --lr 1e-3 --epochs 150 --prune_epoch 50 --lr_adj 1e-2 --wt_para 1e-2 \
--turn_off True
for i in {0..18}; do
    python clean_train.py --dataset cifar100 --lr 1e-3 --epochs 80 --prune_epoch 40 --lr_adj 1e-2 \
    --wt_para 1e-2 --restart_tsk $(($i+1)) --load_model True --model_pth "./cifar100_task_${i}.pt" \
    --turn_off True
done

mv cifar100_task_19.pt ../results/
for i in {0..18}; do
    rm cifar100_task_${i}.pt
done

# python clean_train.py --dataset cifar100 --model cnet --lr 1e-3 --epochs 120 --prune_epoch 30 --lr_adj 1e-2 \
# --wt_para 1e-2 --turn_off True --name exp_cn1 --optim adam --tasks 10 --n_class 10
# for i in {0..8}; do
#     python clean_train.py --dataset cifar100 --model cnet --lr 1e-3 --epochs 80 --prune_epoch 40 --lr_adj 1e-2 \
#     --wt_para 1e-2 --restart_tsk $(($i+1)) --load_model True --model_pth "./exp_cn1cifar100_task_${i}.pt" \
#     --turn_off True --name exp_cn1 --optim adam --tasks 10 --n_class 10 --train_p 90
# done
