# Relevance Mapping Networks
[![PWC](https://img.shields.io/endpoint.svg?url=https://paperswithcode.com/badge/understanding-catastrophic-forgetting-and/continual-learning-on-cifar100-10-tasks)](https://paperswithcode.com/sota/continual-learning-on-cifar100-10-tasks?p=understanding-catastrophic-forgetting-and)

[![PWC](https://img.shields.io/endpoint.svg?url=https://paperswithcode.com/badge/understanding-catastrophic-forgetting-and/continual-learning-on-cifar100-20-tasks)](https://paperswithcode.com/sota/continual-learning-on-cifar100-20-tasks?p=understanding-catastrophic-forgetting-and)

[![PWC](https://img.shields.io/endpoint.svg?url=https://paperswithcode.com/badge/understanding-catastrophic-forgetting-and/continual-learning-on-imagenet-50-5-tasks)](https://paperswithcode.com/sota/continual-learning-on-imagenet-50-5-tasks?p=understanding-catastrophic-forgetting-and)

[![PWC](https://img.shields.io/endpoint.svg?url=https://paperswithcode.com/badge/understanding-catastrophic-forgetting-and/continual-learning-on-permuted-mnist)](https://paperswithcode.com/sota/continual-learning-on-permuted-mnist?p=understanding-catastrophic-forgetting-and)

Implementation of _Understanding Catastrophic Forgetting and Remembering in Continual Learning with Optimal Relevance Mapping_.


Has been tested with Python 3.8/3.9 and PyTorch 1.7.
## Data
Data will be downloaded to the `data` folder for Permuted-MNIST and CIFAR100 experiments.

For Imagenet-50, 50 (10 in each task) random classes (with around 1200 images) need to be selected from the larger Imagenet dataset and copied to the `data` folder with the following directory tree.

    |-- data
        |-- Imagenet50-Cl
            |-- task1
                |-- train
                    |-- 0
                    |-- ...
                    |-- 9
                |-- val
                    |-- 0
                    |-- ...
                    |-- 9
            |-- task2
            |-- task3
            |-- task4
            |-- task5

        
## Supervised Experiments (Catastrophic Forgetting)
The final model for the experiment will be available is `results` folder.
### Permuted-Mnist
`cd code && sh cl_pmnist.sh`
### Split-CIFAR100
For 20 task (with Resnet 18)
`cd code && sh cl_cifar100.sh`

for 10 task
`cd code && sh cl_cnet.sh`
### Imagenet-50
`cd code && sh cl_img50.sh`

Different number of tasks, architectures and hyperparameters can be used for experiments with small (argument) changes in the respective python files. RMN versions of Linear and Convolution layers are present in `main/layers.py`.
## Unsupervised Experiments (Catastrophic Remembering)

## Results
Not much time was spent tuning the hyperparameters and better results can presumably be obtained.
### Note
The current code has been developed with focus on quick and easy implementation and has not been optimised for memory and speed. The following is the current To-Do:

- PyTorch hooks are unstable and thus a much slower, manual alternative has been implemented.  Needs to be fixed for faster implementation.
- The model needs only needs relevance mapping for current task while training - however we instantiated multiple before 1st task for ease of implementation. Needs to be fixed,
- The current relevance mapping needs to be implemented in native binary datatype.  
